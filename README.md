# Python GPIO script launcher

Lanceur de script pour permettre de lancer un script ou un autre au démarrage d'un RPI pour avoir un seul point d'entrée à configurer
Pythjon script launcher depending on GPIO pins configured to launch an associated script.

Work with:
* Raspberry
* OrangePi Zero
* Desktop with simualted GPIO as GpioEmu



## Requirements


* Python 3.4-3.6

* Depending on platform you want to use:
  * OrangePi

```shell
pip install pyA20
```

  * RaspberryPi

```shell
pip install RPi.gpio
```

  * Desktop computer
    * This emulator permit developping simulated GPIO things on computer without having the hardware

```shell
pip install GPIOEmu
```

## Installation

### Script and depdendencies

As this project work for myself, I put here the commands executed to install for my purpose

```shell
git clone https://gitlab.com/zorbac/python_gpio_launcher.git

cd python_gpio_launcher

git clone https://gitlab.com/zorbac/AfficheInfos.git
git clone https://github.com/jingl3s/OrangePiZero_OLED_Dht_display.git

```

* Do not forget to configure the sub project and ensure they work out of this call project

### Auto start main script

* Service launched<br> 

```shell
sudo nano /lib/systemd/system/python_gpio_launcher.service
```

Content<br>
```ini
[Unit]
Description=Python GPIO script launcher
After=multi-user.target
[Service]
Type=idle
ExecStart=/usr/bin/python3 /home/__TBD__/python_gpio_launcher/lanceur.py
[Install]
WantedBy=multi-user.target
``` 

Where
* /usr/bin/python3 is the full path of python binary available with which python3
* __TBD__ is defined path on your installation

```shell
sudo chmod 644 /lib/systemd/system/python_gpio_launcher.service
sudo systemctl daemon-reload
sudo systemctl enable python_gpio_launcher.service
```


## Usage

The main script launcher.py is hard coded to call the other scripts.
At startup, it read the GPIO pins and depending on configuration it launches the corresponding python script

## Schematic example
<img src="schema/cablage_rpi_gpio_launcher.png" alt="Schematic Rpi" width="1000"> </img>
<img src="schema/cablage_orangepi_zero_gpio_launcher.png" alt="Schematic OrangePi" width="1000"> </img>


# Links
* GPIO
  * RaspberryPi https://sourceforge.net/p/raspberry-gpio-python/wiki/Inputs/
  * OrangePi (not used) https://github.com/rm-hull/OPi.GPIO
  
* ETC configuration 
  * https://github.com/laneboysrc/rc-headless-transmitter/blob/master/configurator/orangepizero/INSTALL.md
  

## A faire

* [ ] Extract configuration settings from code like 
  * called script and commands
  * GPIO configuration


## Historique versions

* 1 First version

## Licence

Zorbac

Distributed under licence WTFPL. See ``LICENSE`` for more information.

[https://gitlab.com/zorbac/python_gpio_launcher](https://gitlab.com/zorbac/python_gpio_launcher/)

