#-*-coding:utf8;-*-
'''
@author: zorbac

'''

# license
#
# Producer zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import time
import os
import sys
import subprocess

# Import depending on OrangePi or RapsberryPi or GPIOEmu
try:
    from pyA20.gpio import gpio as GPIO  # @UnresolvedImport
    from pyA20.gpio import port  # @UnresolvedImport
    MODE = "PYA20"
except ImportError:
    MODE = "RPI"
    try:
        import RPi.GPIO as GPIO

    except ImportError:
        import GPIOEmu as GPIO


def io_setup(p_list_pin):
    # Activation du reset pour avoir l'affichage fonctionnel

    # initialize GPIO
    dict_pin = dict()

    if MODE == "PYA20":
        GPIO.init()  # @UndefinedVariable
    elif MODE == "RPI":
        GPIO.setmode(GPIO.BCM)

    for pin in p_list_pin:

        if MODE == "PYA20":
            if pin == 15:
                pa_pin = port.PA15
            elif pin == 16:
                pa_pin = port.PA16
            else:
                raise ValueError("Pin number is not expected {}".format(pin))

            GPIO.setcfg(pa_pin, GPIO.INPUT)  # @UndefinedVariable
            GPIO.pullup(pa_pin, GPIO.PULLUP)  # @UndefinedVariable
        elif MODE == "RPI":
            pa_pin = pin
            GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        else:
            raise ValueError("Pin number is not expected {}".format(pin))

        dict_pin[pin] = dict()
        dict_pin[pin]["pin"] = pa_pin

    print ("attente 3 seconde pour Emultateur")
    time.sleep(3)

    for pin in dict_pin.values():
        current_state = GPIO.input(pin["pin"])
        pin["last_state"] = current_state
        pin["change"] = False
        pin["count"] = 0

    return dict_pin


def main():
    # Dossier de travail
    current_directory = os.path.dirname(__file__)
    current_directory = os.path.realpath(current_directory)
    os.chdir(current_directory)

    # Use same interpreter
    python_cmd = sys.executable

    # Prepare et récupère les informations GPIO
    p_list_pin = list()
    p_list_pin.append(15)
    p_list_pin.append(16)

    for id_enum, pin in enumerate(p_list_pin):
        if id_enum == 0:
            print("PIN {} and default: affiche_info_conf.py OLED".format(pin))
        if id_enum == 1:
            print("PIN {}: disp_oled.py".format(pin))

    dict_pin = io_setup(p_list_pin)

    # Detect les ports actifs
    active = 0
    for pin in p_list_pin:
        if not dict_pin[pin]["last_state"]:
            active = pin

    # Defini les commandes selon la configuration
    list_cmd = list()

    print("active: {}".format(active))

    if active == p_list_pin[0] or active == 0:
        chemin = os.path.realpath(os.path.join(
            current_directory, "AfficheInfos/src/python", "affiche_info_conf.py"))

        if not os.path.exists(chemin):
            raise RuntimeError("Command non existante ".format(chemin))
        list_cmd.append(
            [python_cmd, chemin, "OLED"])

        chemin = os.path.realpath(os.path.join(
            current_directory, "AfficheInfos/src/python/extra", "background_update_wifi.py"))

        if not os.path.exists(chemin):
            raise RuntimeError("Command non existante ".format(chemin))
        list_cmd.append(
            [python_cmd, chemin])

    elif active == p_list_pin[1]:
        chemin = chemin = os.path.realpath(os.path.join(
            current_directory, "OrangePiZero_OLED_Dht_display", "disp_oled.py"))
        if not os.path.exists(chemin):
            raise RuntimeError("Command non existante ".format(chemin))
        list_cmd.append(
            [python_cmd, chemin])
    else:
        raise RuntimeError("No valid push button selected")

    list_popen = list()
    for cmd in list_cmd:
        print("Lancement de {}".format(cmd))
        popen = subprocess.Popen(cmd)
        list_popen.append(popen)

    boucle_processus = True
    while boucle_processus:
        # Detection si un processus est terminé pour arreter tout le monde
        for index, popen in enumerate(list_popen):
            if popen.poll() is not None:
                del list_popen[index]
                boucle_processus = False
        time.sleep(60)

    if not boucle_processus:
        for popen in list_popen:
            popen.kill()

    # Release everything as no need to wait to cleanup thread as they are
    # going to run alone
    exit(0)


if __name__ == '__main__':
    main()
